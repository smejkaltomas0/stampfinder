# import libraries
import constant
import options
from point import Point
import numpy as np


class Stamp:
    """
    class representing the stamp, i.e., 4 points, now only rectangular; TODO: generalize to circles/polygon shapes
    """
    def __init__(self):
        """
        blank constructor. Set value of corners to zero.
        """
        # set corners of the stamp to zero.
        self.bottomLeft = Point(0, 0)
        self.bottomRight = Point(0, 0)
        self.topRight = Point(0, 0)
        self.topLeft = Point(0, 0)

        # set the number of corners.
        self.number_of_corners = 4

        # characteristic_in_spatial_domain / features. 9 features according to the paper:
        # 1,2,3:  the average, medium, standard deviation of the stamp
        # 4: Michelsen contrast
        # 5: brightness to contrast ratio
        # 6-9: Gray-Level Coocurrence Matrix (GLCM) features : contrast, energy, homogeneity, and correlation.
        # this is add in function stampFinder.add_stamps_characteristics_spatial_domain
        self.characteristics_spatial_domain = []

        # img_rgb of the stamp
        # this is add in function stampFinder.add_stamps_characteristics_spatial_domain
        self.img_rgb = []

        # which filter find the stamp: 0 - laplace, 1 - circle, 2 - color; default: -1
        self.used_filter = -1

    def print(self):
        """
        print function
        """
        print("Printing stamp")
        print("bottom left corner: ", end='')
        self.bottomLeft.print()
        print("bottom right corner: ", end='')
        self.bottomRight.print()
        print("top  right corner: ", end='')
        self.topRight.print()
        print("top  left corner: ", end='')
        self.topLeft.print()

    def get_corners(self):
        """
        :return: array of corner points in anti-clockwise order
        """
        return [self.bottomLeft, self.bottomRight, self.topRight, self.topLeft]

    def get_number_of_corners(self):
        """
        :return: return number of corners/points that represent stamp
        """
        return self.number_of_corners

    def get_corner(self, i):
        """
        :param i: id of the corner/point starting with left bottom and going anti-clockwise
        :return: i-th corner/point of the stamp
        """
        if i == 0:
            return self.bottomLeft
        if i == 1:
            return self.bottomRight
        if i == 2:
            return self.topRight
        if i == 3:
            return self.topLeft
        if i == 4:
            return self.get_corner(0)

    def create_from_point_and_distances(self, _x, _y, width, height):
        """
        create stamp from one point and distances
        :param _x: x-coordinate of the left bottom point
        :param _y: y-coordinate of the left bottom point
        :param width: width of the stamp
        :param height: height of the stamp
        """

        # bottom left
        self.bottomLeft.x = _x
        self.bottomLeft.y = _y
        # bottom right
        self.bottomRight.x = _x + width
        self.bottomRight.y = _y
        # top right
        self.topRight.x = _x + width
        self.topRight.y = _y + height
        # top left
        self.topLeft.x = _x
        self.topLeft.y = _y + height

    def create_from_circle(self, _xc, _yc, radius):
        """
        create stamp from circle shape
        Check whether the circle is not overflowing into negative values
        :param _xc: x-coordinate of the center of the circle
        :param _yc: y-coordinate of the center of the circle
        :param radius: radius of the circle
        """
        # bottomLeft point
        self.bottomLeft.x = max(_xc - radius, 0)
        self.bottomLeft.y = max(_yc - radius, 0)
        # bottom right  point
        self.bottomRight.x = _xc + radius
        self.bottomRight.y = max(_yc - radius, 0)
        # top right  point
        self.topRight.x = _xc + radius
        self.topRight.y = _yc + radius
        # top left  point
        self.topLeft.x = max(_xc - radius, 0)
        self.topLeft.y = _yc + radius

    def print_bounding_box(self):
        """
        print the bounding box of the stamp in format [leftBottom_X,leftBottom_y] x [rightTop_x, rightTop_y]
        """
        print("[{}, {}]x[{}, {}]".format(self.bottomLeft.x, self.bottomLeft.y, self.topRight.x, self.topRight.y))

    def ratio(self):
        """
        :return: ratio of the stamp = width/height or height/width that satisfies ratio > 1
        """
        width = self.bottomRight.x - self.bottomLeft.x
        height = self.topLeft.y - self.bottomLeft.y
        ratio = width / height
        if ratio < 1:
            # transform
            return 1.0 / ratio
        else:
            return ratio

    def width(self):
        """
        width of the stamp
        :return:
        """
        return self.bottomRight.x - self.bottomLeft.x

    def height(self):
        """
        height of the stamp
        :return:
        """
        return self.topLeft.y - self.bottomLeft.y

    def area(self):
        """
        :return: area of the stamp
        """
        width = self.bottomRight.x - self.bottomLeft.x
        height = self.topLeft.y - self.bottomLeft.y
        return width*height

    def is_correct(self):
        """
        check the properties of the stamp:
            - ratio - the ratio of the stamp has to be less than constant.MAX_RATIO
        :return: True - the hypothetical stamp could be stamp
                 False - the hypothetical stamp is not a stamp. Discard it!
        """

        if self.ratio() > constant.MAX_RATIO:
            # the ratio is too great. this is not a stamp
            return False

        # TODO: other tests?
        return True

    def is_identical(self, stamp_2, image_width, image_height):
        """
        function for testing whether the stamps are "almost identical".
        How to quantify if they are identical?
        - based on area of intersection - polygonClipping.py. Somewhere error in the implementation. TODO: make it work
        - based on the distances between corners, width and height
        :param stamp_2: second stamp for the comparison
        :param image_width: width of the whole image
        :param image_height: height of the whole image
        :return: true - they are identical
                 false - they are not
        """

        # euklid distance between left-bottom corners
        dist_bottom_left = self.bottomLeft.euklid_norm(stamp_2.bottomLeft)

        # distance between widths of the stamps
        # width has to be transform to float to prevent overflow (negative values)
        diff_width = abs(np.float(self.width()) - np.float(stamp_2.width()))

        # distance between heights of the stamps
        # height has to be transform to float to prevent overflow (negative values)
        diff_height = abs(np.float(self.height() - np.float(stamp_2.height())))

        # define criterion for identical stamps
        crit_dist_bottom_left = 0.05 * max(image_width, image_height)
        crit_diff_width = 0.1 * self.width()
        crit_diff_height = 0.1 * self.height()

        if options.debug:
            print("dist_bottom_left: ", dist_bottom_left, "criterion: ", crit_dist_bottom_left)
            print("diff_width: ", diff_width, "criterion: ", crit_diff_width)
            print("diff_height: ", diff_height, "criterion: ", crit_diff_height)

        if dist_bottom_left < crit_dist_bottom_left \
                and diff_width < crit_diff_width and diff_height < crit_diff_height:
            # if all condition are satisfied, the stamps are identical
            return True
        else:
            # the stamps are not identical
            return False

    def is_overlap(self, stamp_2):
        """
        are two stamps overlapping each other in the 2D plane?
        TODO: is function is_overlap correct? test it!
        :param stamp_2: second stamp
        :return: True: self and stamp_2 overlap
                 False: self and stamp_2 does not overlap
        """

        if self.topRight.y < stamp_2.bottomLeft.y or self.bottomLeft.y > stamp_2.topRight.y:
            # vertically, they do not overlap
            return False
        if self.topRight.x < stamp_2.bottomLeft.x or self.bottomLeft.x > stamp_2.topRight.x:
            # horizontally, they do not overlap
            return False

        return True
