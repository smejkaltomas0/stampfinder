# import libraries
import numpy as np

# static helping functions


def is_clockwise(points, number_of_points):
    """
    check whether the given points are in clockwise order
    :param points: points of the polygon
    :param number_of_points:  number of vertices
    :return: True - the points are in clockwise
             False - the points are not in clockwise
    """
    sign_area = compute_sign_area(points, number_of_points)
    if sign_area > 0:
        return True
    else:
        return False


def compute_sign_area(points, number_points):
    """
    calculate area from points in points using the Shoelace formula
    :param points: points that represent polygon
    :param number_points: number of points
    :return: sign area of the polygon
    """
    area = 0.0

    # calculate value of shoelace formula (https://en.wikipedia.org/wiki/Shoelace_formula)
    j = number_points - 1
    for i in range(number_points):
        x_j = points[j][0]
        x_i = points[i][0]
        y_j = points[j][1]
        y_i = points[i][1]

        # add it to the result
        area += (x_j + x_i) * (y_j - y_i)
        # increase the index
        j = i

    # return value
    return area / 2.0


def x_intersect(x1, y1, x2, y2, x3, y3, x4, y4):
    """
    Returns x-value of point of intersection of two lines.
    First line is represent with points: [x1,y1] and [x2,y2]
    First line is represent with points: [x3,y3] and [x3,y3]
    """
    num = (x1*y2 - y1*x2) * (x3-x4) - (x1-x2) * (x3*y4 - y3*x4)
    den = (x1-x2) * (y3-y4) - (y1-y2) * (x3-x4)
    return num/den


def y_intersect(x1, y1, x2, y2, x3, y3, x4, y4):
    """
    Returns x-value of point of intersection of two lines.
    First line is represent with points: [x1,y1] and [x2,y2]
    First line is represent with points: [x3,y3] and [x3,y3]
    """
    num = (x1*y2 - y1*x2) * (y3-y4) - (y1-y2) * (x3*y4 - y3*x4)
    den = (x1-x2) * (y3-y4) - (y1-y2) * (x3-x4)
    return num/den


class PolygonClipping:
    """
    class for finding the area of intersection of two arbitrary polygons/stamps
    """
    def __init__(self, _stamp_1, _stamp_2):
        """
        constructor; calculate the area of intersection of two polygons
        :param _stamp_1: first polygon/stamp
        :param _stamp_2: second polygon/stamp
        """
        # the area of intersection of the two stamps. set dummy value
        self.area_of_intersection = -1
        # set number  of points of intersection, default value is the
        self.number_of_points_in_intersection = _stamp_1.number_of_corners()
        # stamp are in intersection? default is False
        self.polygon_intersected = False
        # dimension of the problem is 2 -> we work on the plane
        self.dimension = 2
        # number of points is given by the stamps;
        # TODO: generalize for arbitrary number of vertices in both elements (now they have to be equal)
        self.vertices_in_element = _stamp_1.number_of_corners()

        # allocate space
        self.poly_points = np.zeros(shape=(self.vertices_in_element, self.dimension))
        self.clipper_points = np.zeros(shape=(self.vertices_in_element, self.dimension))

        # set the coordinates
        for i in range(_stamp_1.number_of_corners()):
            for j in range(self.dimension):
                self.poly_points[i][j] = _stamp_1.get_corner(i).get_coordinate(j)
                self.clipper_points[i][j] = _stamp_2.get_corner(i).get_coordinate(j)

        # check if the elements are given in ClockWise Order. If not change the orientation
        if not is_clockwise(self.poly_points, self.vertices_in_element):
            # reverse them
            self.change_orientation(self.poly_points, self.vertices_in_element)

        if not is_clockwise(self.clipper_points, self.vertices_in_element):
            # reverse them
            self.change_orientation(self.clipper_points, self.vertices_in_element)

        # calculate the intersection using the sutherland hodgman algorithm.
        # the resulting polygon will be in poly_points
        self.sutherland_hodgman_algorithm()

        # compute area of the result
        self.area_of_intersection = abs(compute_sign_area(self.poly_points, self.number_of_points_in_intersection))

    def change_orientation(self, data_points, number_points):
        """
        CHANGE IN ORIENTATION OF THE POINTS IN data_points
        :param data_points: data with points that will be change
        :param number_points: number of points in points (useless in python TODO: remove number_points)
        :return:
        """
        help_points = np.zeros(shape=(number_points, self.dimension))
        # copy data
        for i in range(number_points):
            # TODO: CHANGE FOR ARBITRARY DIMENSION
            help_points[i][0] = data_points[i][0]
            help_points[i][1] = data_points[i][1]

        # change the direction: the first is identical only the other points has to be change
        for i in range(1, number_points):
            # TODO: CHANGE FOR ARBITRARY DIMENSION
            data_points[i][0] = help_points[number_points - i][0]
            data_points[i][1] = help_points[number_points - i][1]

    def get_area_of_intersection(self):
        """
        :return: area of intersection of two polygons
        """
        return self.area_of_intersection

    def sutherland_hodgman_algorithm(self):
        """
        implementation of the sutherland hodgman algorithm for clipping
        https://en.wikipedia.org/wiki/Sutherland%E2%80%93Hodgman_algorithm
        """
        # define clipper_size
        clipper_size = self.vertices_in_element

        # i and k are two consecutive indexes
        for i in range(clipper_size):
            k = (i+1) % clipper_size
            # We pass the current array of vertices, it's size
            # and the end points of the selected clipper line
            self.clip(i, k)

    def clip(self, l, m):
        """
        This functions clips all the edges w.r.t one clip edge of clipping area
        :param l: first edge
        :param m: second edge
        :return:
        """

        x1 = self.clipper_points[l][0]
        y1 = self.clipper_points[l][1]

        x2 = self.clipper_points[m][0]
        y2 = self.clipper_points[m][1]

        # clip algorithm
        max_points = 20
        new_points = np.zeros(shape=(max_points, self.dimension))
        new_poly_size = 0

        # (ix,iy),(kx,ky) are the co-ordinate values of the points
        for i in range(self.number_of_points_in_intersection):
            # i and k form a line in polygon
            k = (i+1) % self.number_of_points_in_intersection
            ix = self.poly_points[i][0]
            iy = self.poly_points[i][1]
            kx = self.poly_points[k][0]
            ky = self.poly_points[k][1]

            # calculating position of first point w.r.t. clipper line
            i_pos = (x2-x1) * (iy-y1) - (y2-y1) * (ix-x1)

            # calculating position of second point w.r.t. clipper line
            k_pos = (x2-x1) * (ky-y1) - (y2-y1) * (kx-x1)

            # set to zero if small enough
            if abs(i_pos) < 1e-10:
                i_pos = 0.0
            if abs(k_pos) < 1e-10:
                k_pos = 0.0

            # Case 1 : When both points are inside
            if (i_pos < 0) and (k_pos < 0):
                # Only second point is added
                new_points[new_poly_size][0] = kx
                new_points[new_poly_size][1] = ky
                new_poly_size += 1

                # set intersection to true:
                self.polygon_intersected = True

            # Case 2: When only first point is outside
            elif (i_pos >= 0) and (k_pos < 0):
                # Point of intersection with edge and the second point is added
                new_points[new_poly_size][0] = x_intersect(x1, y1, x2, y2, ix, iy, kx, ky)
                new_points[new_poly_size][1] = y_intersect(x1, y1, x2, y2, ix, iy, kx, ky)
                new_poly_size += 1

                new_points[new_poly_size][0] = kx
                new_points[new_poly_size][1] = ky
                new_poly_size += 1

                # set intersection to true:
                self.polygon_intersected = True

            # Case 3: When only second point is outside
            elif (i_pos < 0) and (k_pos >= 0):
                # Only point of intersection with edge is added
                new_points[new_poly_size][0] = x_intersect(x1, y1, x2, y2, ix, iy, kx, ky)
                new_points[new_poly_size][1] = y_intersect(x1, y1, x2, y2, ix, iy, kx, ky)
                new_poly_size += 1

                # set intersection to true:
                self.polygon_intersected = True

            # Case 4: When both points are outside
            # do nothing

        # Copying new points into original array and changing the no. of vertices
        self.number_of_points_in_intersection = new_poly_size
        for i in range(self.number_of_points_in_intersection):
            self.poly_points[i][0] = new_points[i][0]
            self.poly_points[i][1] = new_points[i][1]
