###########################
# CONSTANT FOR THE FINDER #
###########################

# colors
COLOR_RED = (0, 0, 255)
COLOR_BLUE = (255, 0, 0)
COLOR_GREEN = (0, 255, 0)
COLOR_WHITE = (255, 255, 255)
COLOR_BLACK = (0, 0, 0)

# classification of the stamps
# maximum value of the ratio of the bounding box
MAX_RATIO = 2.8
# min/max value in % / 100 of the perimeter  of the stamp with respect to the total perimeter of the image
# see function StampFinder.mask_to_stamp(...)
MIN_PERIMETER = 0.001
MAX_PERIMETER = 0.3
# min/max value in % / 100 of the area of the stamp with respect to the total perimeter of the image
# see function StampFinder.mask_to_stamp(...)
MIN_AREA = 0.001
MAX_AREA = 0.1


