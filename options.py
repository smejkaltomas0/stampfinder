############################
# OPTION FOR THE FINDER ####
############################

# debug mode for printing information
debug = False

# check identical stamps to discard occurrences of two filters find identical stamps
check_identical = True

# select which finder to use
use_laplace = False
use_circle = False
use_color = True

# specify which palette use in color filter
# only relevant of use_color == True
use_color_hsv_palette = True
use_color_ycrcb_palette = True

# use machine learning
use_machine_learning = True
# choose classifier
# 0 - svm, 1 - 1NN, 2 - 3NN, 3-5NN
classifier = 2

# smaller the image in the grayLevelCoocurrenceMatrix to speed-up the computation
smaller_glc_matrix = True
