# -----------------------------------------------------------
# stamp finder for documents
# the settings of the finder can be adjusted in the options.py file
# numerical adjustable constant are defined in constant.py file
# how to run:
# in main.py:
# 1. define the folder where the images are: set variable 'dir_with_files' (line 39)
# 2. define the output folder: set variable 'dir_save' (line 41)
# 3. run main.py script
#
# output:
# array of stamps;
# images with mark stamps will be written in the 'dir_save' directory
# the corresponding bounding boxes can be printed with 'print_bounding_box()' function
# (C) 2021 Tomas Smejkal, Prague, Czechia
# email tomas.smejkal@fjfi.cvut.cz
# -----------------------------------------------------------

# import libraries and other classes
import cv2.cv2 as cv
import os
import time
import stampFinder
import options
from stampFinder import StampFinder

###################################################################
# step 0: if the finder uses machine learning, set the classifier #
###################################################################
if options.use_machine_learning:
    # create training set
    x, y = stampFinder.create_training_set("training_stamps/stamps", "training_stamps/no_stamps")
    classifier = stampFinder.choose_classifier()
    # fit data
    classifier.fit(x, y)
else:
    # dummy classifier
    classifier = []

#################################
# step 1: define paths to files #
#################################
# define the folder with files
dir_with_files = "dataset/stamps"
# define the folder for saving results
dir_save = "out"
# check whether given save dir exists; if not create it
if not os.path.exists(dir_save):
    os.mkdir(dir_save)

##################################################################
# step 2: for all files in the dir_with_files execute the script #
##################################################################
files = os.listdir(dir_with_files)
for f in files:
    path_to_file = dir_with_files + "/" + f
    print("Finding stamps in", path_to_file)
    # start the clock to measure time
    start = time.time()
    # create instance of the stamp finder class
    s = StampFinder(path_to_file)
    # run the finder with the given classifier from step 0
    result = s.run(classifier)

    print("The finder found", len(result), "stamps. The bounding boxes are")
    for stamp in result:
        stamp.print_bounding_box()

    # draw image with stamps
    image = stampFinder.draw_image_with_stamps(path_to_file, result)
    # save image with stamps
    path_to_save = dir_save + "/stamps_" + f
    cv.imwrite(path_to_save, image)
    # stop the clock to measure time
    end = time.time()
    print("Computation time: {} [s]".format(end - start))
    print("+++++++++++++++++++++++++++++++++++++++++++++")

# wait for key stroke to end the program
cv.waitKey(0)
cv.destroyAllWindows()
