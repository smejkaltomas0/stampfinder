# import libraries
import skimage.feature as sk
import numpy as np
import options


class GrayLevelCoocurenceMatrix:
    """
    class for computing the gray level co-occurrence matrix
    https://en.wikipedia.org/wiki/Co-occurrence_matrix
    wiki cite: A co-occurrence matrix or co-occurrence distribution (gray-level co-occurrence matrices GLCMs) is a
    matrix that is defined over an image to be the distribution of co-occurring pixel values
    (grayscale values, or colors) at a given offset. It is used as an approach to texture analysis.
    """
    def __init__(self, img_gray):
        """
        constructor. Compute the glc matrix and its features of gray-scale image in img_gray.
        The computation is based on:
         https://scikit-image.org/docs/dev/api/skimage.feature.html#skimage.feature.graycomatrix
         https://stackoverflow.com/questions/19556538/how-to-find-glcm-of-an-image-in-opencv-or-numpy
        :param img_gray: gray-scale image
        """
        if options.smaller_glc_matrix:
            # reduction of the problem size - initially 256x256 to expensive. Therefore, we decrease nb of colors
            n_levels = 256
            n_bins = 4
            img_gray_for_glcm = np.uint8(np.digitize(img_gray, np.arange(0, n_levels, n_bins))) - 1
        else:
            # do nothing. The matrix will no be resized.
            img_gray_for_glcm = img_gray

        # compute the glcm matrix
        result = sk.greycomatrix(img_gray_for_glcm, [1], [0], levels=img_gray_for_glcm.max() + 1)
        self.glcm_matrix = result[:, :, 0, 0]
        self.size_matrix = self.glcm_matrix.shape[0]

        # compute the features
        self.contrast = self.compute_contrast()
        self.energy = self.compute_energy()
        self.homogeneity = self.compute_homogeneity()
        self.correlation = self.compute_correlation()

    def compute_contrast(self):
        """
        compute the contrast of the glc matrix. This matrix has been already computed in the constructor.
        :return: result: the computed contrast
        """
        result = 0.0
        for p in range(self.size_matrix):
            for q in range(self.size_matrix):
                result += (p-1) * (p-1) * self.glcm_matrix[p, q]

        return result

    def compute_energy(self):
        """
        compute the energy of the glc matrix. This matrix has been already computed in the constructor.
        :return: result: the computed GLCM energy
        """
        result = 0.0
        for p in range(self.size_matrix):
            for q in range(self.size_matrix):
                result += np.float(self.glcm_matrix[p, q]) * np.float(self.glcm_matrix[p, q])

        return result

    def compute_homogeneity(self):
        """
        compute the homogeneity of the glc matrix. This matrix has been already computed in the constructor.
        :return: result: the computed GLCM homogeneity
        """
        result = 0.0
        for p in range(self.size_matrix):
            for q in range(self.size_matrix):
                result += self.glcm_matrix[p, q] / (1 + abs(np.float(p) - np.float(q)))

        return result

    def compute_correlation(self):
        """
        compute the correlation of the glc matrix. This matrix has been already computed in the constructor.
        :return: result: the computed GLCM correlation
        """
        result = 0.0
        for p in range(self.size_matrix):
            # compute man and deviation of the p-row
            mu_p = np.mean(self.glcm_matrix[p, :])
            sigma_p = np.std(self.glcm_matrix[p, :])
            for q in range(self.size_matrix):
                # compute mean and deviation of the q column
                mu_q = np.mean(self.glcm_matrix[:, q])
                sigma_q = np.std(self.glcm_matrix[:, q:])
                result += self.glcm_matrix[p, q] * (np.float(p) - np.float(mu_p)) * (np.float(q) - np.float(mu_q)) \
                    / (1 + np.float(sigma_p) * np.float(sigma_q))

        return result
