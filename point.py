# import libraries
import math
import sys
import numpy as np


class Point:
    """
    class representing points in 2D
    attributes:
    """

    def __init__(self, _x, _y):
        """
        constructor
        :param _x: x-coordinate
        :param _y: y-coordinate
        """

        # set the x and y coordinate
        self.x = _x
        self.y = _y

    def print(self):
        """
        print function
        """
        print("[", self.x, ",", self.y, "]")

    def get_coordinate(self, i):
        """
        get i-th coordinate
        :param i:
        :return: i-th coordinate
        """
        if i == 0:
            return self.x
        if i == 1:
            return self.y

        # error message
        sys.exit("point::get_coordinate(self,i): unknown index, given: " + str(i))

    def euklid_norm(self, second_point):
        """
        measure the Euklid (L2) distance between self point and given point
        :param second_point: second point
        :return: distance between self and second_point
        """
        distance = math.sqrt(
            (np.float(self.x) - np.float(second_point.x)) ** 2 + (np.float(self.y) - np.float(second_point.y)) ** 2)

        return distance
