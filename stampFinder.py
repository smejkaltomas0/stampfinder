####################
# IMPORT LIBRARIES #
####################
# computer vision , matrix library, qr codes, and machine learning library
import cv2.cv2 as cv
import numpy as np
from pyzbar.pyzbar import decode
from sklearn import svm, neighbors
from os.path import exists
import sys
import os

# constants and options for the finder
import constant
import options

# other classes
from grayLevelCoocurrenceMatrix import GrayLevelCoocurenceMatrix
from stamp import Stamp


"""
helping static functions for the stamp finder
"""


def choose_classifier():
    """
    choose classifier based on the option set in the option.py file
    currently there are 4 choices of the classifier
    """
    if options.classifier == 0:
        classifier = svm.SVC()
    elif options.classifier == 1:
        classifier = neighbors.KNeighborsClassifier(n_neighbors=1)
    elif options.classifier == 2:
        classifier = neighbors.KNeighborsClassifier(n_neighbors=3)
    elif options.classifier == 3:
        classifier = neighbors.KNeighborsClassifier(n_neighbors=5)
    else:
        sys.exit("Unknown classifier defined in options.classifier. Given: " + options.classifier)

    return classifier


def create_training_set(dir_training_stamps, dir_training_nostamps):
    """
    create training set for the classifier. Stamps will be class 1, no-stamps will be class 0
    :param dir_training_stamps: directory with stamps,
    :param dir_training_nostamps: directory with no-stamps,
    :return: X - array of arrays with x-values = characteristics of stamps/no_stamps in the spatial domain
             Y - array of ones and zeroes (stamp vs. no_stamp)
    """
    x = []
    y = []
    # first add stamps to the training set
    files = os.listdir(dir_training_stamps)
    for f in files:
        path_to_file = dir_training_stamps + "/" + f
        img_stamp = cv.imread(path_to_file)
        # compute features of the stamp
        features = objects_characteristics_spatial_domain(img_stamp)
        # append it to the result
        x.append(features)
        y.append(1)
    # second add no-stamps to the training set
    files = os.listdir(dir_training_nostamps)
    for f in files:
        path_to_file = dir_training_nostamps + "/" + f
        img_stamp = cv.imread(path_to_file)
        features = objects_characteristics_spatial_domain(img_stamp)
        x.append(features)
        y.append(0)
    # return the arrays of the training set
    return x, y


def draw_image_with_stamps(path_to_file, stamps):
    """
    :param path_to_file: path to the file where the stamps will be drawn
    :param stamps: array of stamps
    :return: img - image with stamps
    """

    # read image where the stamps will be drawn
    img = cv.imread(path_to_file)
    for i in range(len(stamps)):
        # for each stamp draw a rectangle using points from stamp:
        stamp = stamps[i]

        # properties of the lines that marks the stamps
        # color is based on which filter find the stamp
        if stamps[i].used_filter == 0:
            color = constant.COLOR_RED
        elif stamps[i].used_filter == 1:
            color = constant.COLOR_GREEN
        elif stamps[i].used_filter == 2:
            color = constant.COLOR_BLUE
        else:
            color = constant.COLOR_BLACK
            print("Warning: something went wrong. Unknown filter in draw_image_with_stamps. "
                  "Given: ", stamps[i].used_filter)

        # choose thickness of the rectangle
        thickness = 4
        # draw the rectangle using the corners
        for j in range(stamp.get_number_of_corners()):
            start_point = stamp.get_corner(j)
            end_point = stamp.get_corner(j + 1)
            # draw line from start_point to end_point
            img = cv.line(img, (start_point.x, start_point.y), (end_point.x, end_point.y), color, thickness)

    # draw text with information about the filter
    if options.debug:
        cv.putText(img, 'Red = laplace_filter', (10, 100), cv.FONT_HERSHEY_SIMPLEX, 3, constant.COLOR_RED, 2, cv.LINE_AA)
        cv.putText(img, 'Green = circle_filter', (10, 200), cv.FONT_HERSHEY_SIMPLEX, 3, constant.COLOR_GREEN, 2, cv.LINE_AA)
        cv.putText(img, 'Blue = color_filter', (10, 300), cv.FONT_HERSHEY_SIMPLEX, 3, constant.COLOR_BLUE, 2, cv.LINE_AA)

    # return the result
    return img


def create_rectangle_of_given_color(image, x, y, width, height, color):
    """
    create rectangle with a given color in a given place
    params:
        :param image:   matrix of the image in arbitrary format; the rectangle will be drawn into this image (not const)
        :param x:       x-position of the left-bottom corner of the rectangle
        :param y:       y-position of the left-bottom corner of the rectangle
        :param width:   width of the rectangle
        :param height:  height of the rectangle
        :param color:   color of the rectangle
    """

    # check whether image and color has identical number of channels
    if image.shape[2] != len(color):
        # the sizes does not match;
        sys.exit("Error in stampFinder.create_rectangle_of_given_color(...):  \
            number of channels of given color and image does not match!")

    # for each channel, redefine the image
    for i in range(len(color)):
        image[y:y + height, x:x + width, i] = color[i]


def auto_canny(gray_image, sigma=0.33):
    """
    Canny detector of edges using the cv Canny detector
    :param gray_image: given image in gray scale
    :param sigma:  parameter of the deterctor, default value = 0.33
    :return: edged: the edges of the gray_image
    """

    # compute the median of the single channel pixel intensities
    v = np.median(gray_image)
    # apply automatic Canny edge detection using the computed median
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv.Canny(gray_image, lower, upper)
    # return the edged image
    return edged


def fill_holes_closing(binary_image):
    """
    using morphology operations, fill holes in the mask
    :param binary_image: mask  with blobs that have holes
    :return: closing: mask with blobs without holes
    """

    # first open the image to get rid of singular points/small blobs
    kernel_opening = cv.getStructuringElement(cv.MORPH_RECT, (5, 5))
    open_image = cv.morphologyEx(binary_image, cv.MORPH_OPEN, kernel_opening)

    # get size of the image
    height, width = binary_image.shape[:2]
    # kernel size is proportional to the size of the image
    kernel_size = (width // 20, height // 20)
    kernel = cv.getStructuringElement(cv.MORPH_RECT, kernel_size)

    # close the open image to get rid of holes
    closing = cv.morphologyEx(open_image, cv.MORPH_CLOSE, kernel)

    # return the resulting closed image
    return closing


def extract_file_name_from_path(path, with_extension):
    """
    extract file  name from full path using split function.
    :param path: full path to the file
    :param with_extension:  true -> return with extension,
                            false - return without extension
    :return: string with filename with or without extension
    """

    # split path by '/'
    split = path.split('/')
    # filename is the last element of the array
    filename_with_extension = split[-1]
    if with_extension:
        return filename_with_extension
    else:
        # crop the extension
        return filename_with_extension.split('.')[0]


def objects_characteristics_spatial_domain(img_rgb):
    """
    compute the object characteristic of the given image.
    :param img_rgb: image in RGB format
    :return: 1-3: average, standard deviation, and median of pixel intensity,
             4-5: Michelsen constrast, brightess to contrast ratio,
             6-9: glcm features
    """
    # save the image in different palettes
    img_gray = cv.cvtColor(img_rgb, cv.COLOR_BGR2GRAY)
    img_ycrcb = cv.cvtColor(img_rgb, cv.COLOR_BGR2YCR_CB)
    img_c_r = img_ycrcb[:, :, 1]
    img_c_b = img_ycrcb[:, :, 2]

    # 1-3: statistical values for image intensity: mean, standard deviation, and median
    # compute it in individual channels
    img_vector_r = img_c_r.flatten()
    img_vector_b = img_c_b.flatten()

    f_avg_r = np.mean(img_vector_r)
    f_std_r = np.std(img_vector_r)
    f_med_r = np.median(img_vector_r)

    f_avg_b = np.mean(img_vector_b)
    f_std_b = np.std(img_vector_b)
    f_med_b = np.median(img_vector_b)

    # compute it in gray scale
    img_vector_gray = img_gray.flatten()
    f_avg = np.mean(img_vector_gray)
    f_std = np.std(img_vector_gray)
    f_med = np.median(img_vector_gray)

    # 4: Michelsen contrast
    # get luminescence
    y_channel = cv.cvtColor(img_rgb, cv.COLOR_BGR2YUV)[:, :, 0]
    # compute min and max of y_channel
    y_min = np.min(y_channel)
    y_max = np.max(y_channel)
    # compute contrast
    contrast = (np.float(y_max) - np.float(y_min)) / (np.float(y_max) + np.float(y_min))

    # 5: brightness_contrast_ratio
    img_brightness = (img_rgb[:, :, 0] + img_rgb[:, :, 1] + img_rgb[:, :, 2]) / 3
    # mean brightness
    mean_brightness = np.mean(img_brightness.flatten())
    brightness_contrast_ratio = mean_brightness / contrast

    # 6-9: Gray-Level Coocurrence Matrix (GLCM) features : contrast, correlation, energy, and homogeneity.
    # https://scikit-image.org/docs/dev/api/skimage.feature.html#skimage.feature.graycomatrix
    # https://stackoverflow.com/questions/19556538/how-to-find-glcm-of-an-image-in-opencv-or-numpy
    glcm = GrayLevelCoocurenceMatrix(img_gray)

    # return the result
    # what intensity values used? In gray-scale or in individual channels? Currently the individual channels are used
    image_intensity_in_gray = False
    if not image_intensity_in_gray:
        return f_avg_r, f_std_r, f_med_r, f_avg_b, f_std_b, f_med_b, \
               contrast, brightness_contrast_ratio, \
               glcm.contrast, glcm.energy, glcm.homogeneity, glcm.correlation
    else:
        return f_avg, f_std, f_med, \
            contrast, brightness_contrast_ratio, \
            glcm.contrast, glcm.energy, glcm.homogeneity, glcm.correlation


class StampFinder:
    """
    class for solve the stamp finding problem
    """
    def __init__(self, path):
        """
        constructor where the main steps are performed:
        1. load the image using cv.imread function
        2. preprocessed the image (destroy unwanted features such as qr codes, bar codes)
        3. save preprocessed image in different formats (gray, HSC,YCbCr)
        4. save properties of the image which are needed for later use (area or perimeter)
        5. prepare array with resulting stamps
        :param path: path to given image
        """
        #################
        # 1. load image #
        #################
        self.pathToFile = path
        self.filename_without_extension = extract_file_name_from_path(path, False)
        #  check whether the file even exists. If not, exit the program
        if not exists(self.pathToFile):
            # file does not exists. exit the program
            sys.exit("File '" + self.pathToFile + "'  does not exist! The program is terminated.")

        # save image in RGB
        self.img_rgb = cv.imread(self.pathToFile)
        # save raw image without any changes (changes will be done in the preprocessing
        self.img_rgb_raw = self.img_rgb.copy()

        ####################
        # 2. preprocessing #
        ####################
        # destroy qr codes and bar codes
        self.destroy_qr_codes_and_bar_codes()

        ############################
        # 3. save in other formats #
        ############################
        self.img_hsv = cv.cvtColor(self.img_rgb, cv.COLOR_BGR2HSV)
        self.img_ycrcb = cv.cvtColor(self.img_rgb, cv.COLOR_BGR2YCR_CB)
        self.img_gray = cv.cvtColor(self.img_rgb, cv.COLOR_BGR2GRAY)

        ##############################
        # 4. properties of the image #
        ##############################
        self.img_area = self.img_rgb.shape[0] * self.img_rgb.shape[1]
        self.img_perimeter = 2 * (self.img_rgb.shape[0] + self.img_rgb.shape[1])
        self.img_width = self.img_rgb.shape[1]
        self.img_height = self.img_rgb.shape[0]

        #######################################
        # 5. prepare result array with stamps #
        #######################################
        # array of stamps
        self.stamps = []

###############################
# run function to find stamps #
###############################
    def run(self, classifier):
        """
        function to run the computation with image store in self.img_rgb
        :param classifier: if the machine learning option is turn on, this is the trained classifier
        :return: result:  array of find stamps
        """
        # step 1: based on different filters find candidates for stamps

        # find stamps based on the color
        if options.use_color:
            self.color_based_finder()
        # find stamps based on the shape
        if options.use_circle:
            self.circle_based_finder()
        # find stamps based on the laplace operator
        if options.use_laplace:
            self.laplace_based_finder()

        if options.use_machine_learning:
            # step 2: add features of the find candidates
            self.add_stamps_characteristics_spatial_domain()

            # step 3: use classifier to delete no-stamps elements in result
            self.remove_nonstamp_using_classification(classifier)

        # return the found stamps array
        return self.stamps

##########################
# PREPROCESSED FUNCTIONS #
##########################
    def destroy_qr_codes_and_bar_codes(self):
        """
        stamps are easily misread as qr coded or bar codes. Find these object and replace them with write space. Change
        the load img_RGB image
        """
        # find bar codes and qr codes using the py library. OpenCV can be used as well, but is not as good
        result = decode(self.img_rgb)
        if options.debug:
            print("Searched and destroyed:", len(result), " qr codes and bar codes")

        # each qr/bar code replace with white color
        for i in range(len(result)):
            obj = result[i]
            create_rectangle_of_given_color(self.img_rgb, obj.rect.left, obj.rect.top, obj.rect.width,
                                            obj.rect.height, constant.COLOR_WHITE)

################################
# main functions for detection #
################################
    def laplace_based_finder(self):
        """
        find stamps in self.img_rgb based on the laplace filter
        :return:
        """

        # use image in gray scale
        gray = self.img_gray

        # equalize lighting
        clahe = cv.createCLAHE(clipLimit=2.0, tileGridSize=(8, 8))
        gray = clahe.apply(gray)

        # edge enhancement based on Laplace operator
        edge_enh = cv.Laplacian(gray, ddepth=cv.CV_8U, ksize=3, scale=1, delta=0)

        # bilateral blur, which keeps edges
        blurred = cv.bilateralFilter(edge_enh, 13, 50, 50)

        # use simple thresholding
        _, thresh = cv.threshold(blurred, 40, 255, cv.THRESH_BINARY)

        # do some morphology to isolate the stamps blobs
        kernel = cv.getStructuringElement(cv.MORPH_RECT, (15, 15))
        closed = cv.morphologyEx(thresh, cv.MORPH_CLOSE, kernel)
        closed = cv.erode(closed, None, iterations=1)
        closed = cv.dilate(closed, None, iterations=5)

        # transform the mask to stamps. In this function the found stamps are added to self.result
        self.mask_to_stamp(closed, 0)

    def color_based_finder(self):
        """
        Since most of the stamps are in red or blue channel transform the RGB image to YCbCr/HSV
        Then we apply closing morphology and see the first suggestion for stamps
        :return:
        """
        if options.use_color_hsv_palette:
            # find red and blue regions with HSV palette
            mask_red, mask_blue = self.red_and_blue_colors_using_hsv()
            # fill holes using morphology:
            mask_red_without_holes = fill_holes_closing(mask_red)
            mask_blue_without_holes = fill_holes_closing(mask_blue)
            # in debug print the masks
            if options.debug:
                cv.imshow('mask_red_hsv_without_holes', cv.resize(mask_red_without_holes, (500, 900)))
                cv.imshow('mask_blue_hsv_without_holes', cv.resize(mask_blue_without_holes, (500, 900)))
            # add to the result by transforming the mask to stamps.
            self.mask_to_stamp(mask_red_without_holes, 2)
            self.mask_to_stamp(mask_blue_without_holes, 2)

        if options.use_color_ycrcb_palette:
            # find red and blue regions with ycrcb palette
            mask_red, mask_blue = self.red_and_blue_colors_using_ycrcb()
            # fill holes using morphology
            mask_red_without_holes = fill_holes_closing(mask_red)
            mask_blue_without_holes = fill_holes_closing(mask_blue)
            # in debug print values
            if options.debug:
                cv.imshow('mask_red_ycrcb_without_holes', cv.resize(mask_red_without_holes, (500, 900)))
                cv.imshow('mask_blue_ycrcb_without_holes', cv.resize(mask_blue_without_holes, (500, 900)))
            # add to the result by transforming the mask to stamps.
            self.mask_to_stamp(mask_red_without_holes, 2)
            self.mask_to_stamp(mask_blue_without_holes, 2)

    def circle_based_finder(self):
        """
        Since many stamps has circular character, find all circular object in the image using the cv library and
        Houng circle function
        :return:
        """

        # blur using 3 * 3 kernel.
        gray_blurred = cv.blur(self.img_gray, (3, 3))

        # apply Hough transform on the blurred image to detect the circles
        detected_circles = cv.HoughCircles(gray_blurred,
                                           cv.HOUGH_GRADIENT, 1, 20, param1=50,
                                           param2=85, minRadius=40, maxRadius=200)

        # if some circles are detected, convert them to stamps
        if detected_circles is not None:

            # Convert the circle parameters a, b and r to integers.
            detected_circles = np.uint16(np.around(detected_circles))
            # for each circle, create a new stamp
            for pt in detected_circles[0, :]:
                # get center of circle (a,b) and the radius r
                a, b, r = pt[0], pt[1], pt[2]
                if options.debug:
                    print("Find circular object width parameters a,b,r: ", a, b, r)

                # transform circle -> stamp
                new_stamp = Stamp()
                new_stamp.create_from_circle(a, b, r)
                new_stamp.used_filter = 1

                # check the ratio or other properties of the stamp
                if new_stamp.is_correct():
                    # check whether identical stamp exists
                    if not self.identical_stamp_already_exists(new_stamp):
                        # we have new unique stamp
                        self.stamps.append(new_stamp)

    def remove_nonstamp_using_classification(self, classifier):
        """
        from self.result remove stamps that did not pass machine learning test/classification
        :param classifier: choose classifier with already fitted data
        :return:
        """
        # array of no-stamp indices
        no_stamp = []
        # predict if the stamp is stamp
        for i in range(len(self.stamps)):
            found_class = classifier.predict([self.stamps[i].characteristics_spatial_domain])
            if found_class[0] == 0:
                # found stamp is not a stamp!
                if options.debug:
                    print("stamp {} is not a stamp!".format(i))
                no_stamp.append(i)

        # remove non_stamp stamp from the result
        self.stamps = np.delete(self.stamps, no_stamp)

    def add_stamps_characteristics_spatial_domain(self):
        """
        for each stamp in self.stamps find the features/characteristics for classification
        currently 9 features are implemented:
        1,2,3:  the average, medium, and standard deviation of pixel intensity
        4: Michelsen constrast
        5: brightness to contrast ratio
        6-9: Gray-Level Coocurrence Matrix (GLCM) features : contrast, energy, homogeneity, and correlation.
        """
        for i in range(len(self.stamps)):
            # crop stamp from the image
            img_stamp = self.crop_stamp_from_image(self.stamps[i])
            # add to the stamp
            self.stamps[i].img_rgb = img_stamp
            # compute characteristics and store them in stamps
            self.stamps[i].characteristics_spatial_domain = objects_characteristics_spatial_domain(img_stamp)

            if options.debug:
                cv.imshow('stamp_' + str(i), cv.resize(img_stamp, (500, 900)))
                print("features: ", self.stamps[i].characteristics_spatial_domain)

#####################
# helping functions #
#####################
    def crop_stamp_from_image(self, stamp):
        """
        crop the stamp from the RGB image
        :param stamp: stamp that is crop from the image
        :return: image (np array of the stamp)
        """
        # get position of the cropped image
        x_position = stamp.bottomLeft.x
        y_position = stamp.bottomRight.y
        width = stamp.width()
        height = stamp.height()
        # cropped the image
        cropped_image = self.img_rgb[y_position:y_position + height, x_position:x_position + width]

        return cropped_image

    def identical_stamp_already_exists(self, new_stamp):
        """
        iterate through each stamp in self.stamps and check whether similar stamp as new_stamp is already present
        :param new_stamp: candidate for a new stamp
        :return: True - there is identical stamp in self.stamps
                 False - the candidate represent new stamp
        """
        # should we use this control at all?
        if not options.check_identical:
            return False

        for i in range(len(self.stamps)):
            # get the i-th stamp
            stamp = self.stamps[i]
            if new_stamp.is_identical(stamp, self.img_width, self.img_height):
                # they are mostly identical. no need to check others
                return True

        # there is no other similar stamp, return false
        return False

    def red_and_blue_colors_using_ycrcb(self):
        """
        find red and blue colors in the image using the YCrCb palette
        :return: mask_red, mask_blue: mask with red and blue colors
        """
        # extract individual channels, the first one is not used
        c_r = self.img_ycrcb[:, :, 1]
        c_b = self.img_ycrcb[:, :, 2]

        # create mask for the red color
        mask_red = c_r
        mask_red[mask_red == 128] = 0
        mask_red[mask_red > 0] = 255

        # create mask the blue color
        mask_blue = c_b
        mask_blue[mask_blue == 128] = 0
        mask_blue[mask_blue > 0] = 255

        # show masks in the debug version
        if options.debug:
            cv.imshow('mask_red', cv.resize(mask_red, (500, 900)))
            cv.imshow('mask_blue', cv.resize(mask_blue, (500, 900)))

        return mask_red, mask_blue

    def red_and_blue_colors_using_hsv(self):
        """
         find red and blue colors in the image using the HSV palette
        :return: mask_red, mask_blue: mask with red and blue colors
         """

        # red color:
        # In HSV space, the red color wraps around 180. So H values to be both in [0,10] and [170, 180]
        red_low_1 = np.array([0, 100, 20])
        red_top_1 = np.array([10, 255, 255])
        mask_1 = cv.inRange(self.img_hsv, red_low_1, red_top_1)

        red_low_2 = np.array([160, 100, 20])
        red_top_2 = np.array([179, 255, 255])
        mask_2 = cv.inRange(self.img_hsv, red_low_2, red_top_2)

        # the result is or
        mask_red = mask_1 | mask_2

        # blue_color
        # define range blue color in HSV these values are reasonable good
        blue_l = np.array([94, 80, 2])
        blue_u = np.array([126, 255, 255])
        # create mask using inRange function
        mask_blue = cv.inRange(self.img_hsv, blue_l, blue_u)

        return mask_red, mask_blue

    def mask_to_stamp(self, mask, which_filter):
        """
        Transform the mask to stamps. Using contours find the biggest areas with a constant values and then save the
        stamps to the class variable. Perform some classification if the find blob is really a stamp (based on size)
        :param mask: mask of the find blobs of the figure
        :param which_filter: which filter was use to find the stamp
        :return:
        """
        # find contours of the mask
        contours, hierarchy = cv.findContours(mask, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)

        # in the debug mode, print the number of blobs/stamps
        if options.debug:
            print("Number of found contours: ", len(contours))

        # for each blob - make a rectangle
        for cnt in contours:

            # size check
            area = cv.contourArea(cnt)
            perimeter = cv.arcLength(cnt, True)
            if options.debug:
                print("area_cnt:", area, "(% of image: ", area / self.img_area * 100)
                print("perimeter_cnt:", perimeter, "(% of image: ", perimeter / self.img_perimeter * 100)

            if constant.MIN_PERIMETER * self.img_perimeter < perimeter < constant.MAX_PERIMETER * self.img_perimeter \
                    and constant.MIN_AREA * self.img_area < area < constant.MAX_AREA * self.img_area:
                # we have good properties
                # make rectangle = bounding box
                x, y, w, h = cv.boundingRect(cnt)
                # create a stamp from x,y,w,h
                new_stamp = Stamp()
                new_stamp.create_from_point_and_distances(x, y, w, h)
                new_stamp.used_filter = which_filter
                # append the found stamp to the result if it is correct - some more classification (e.g., ratio)
                if new_stamp.is_correct():
                    # also check whether similar stamp is not already in the self.stamps
                    if not self.identical_stamp_already_exists(new_stamp):
                        # we have a new unique stamp - add it!
                        self.stamps.append(new_stamp)
